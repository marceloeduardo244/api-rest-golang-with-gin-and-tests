package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/marceloeduardo244/api-rest-golang-with-gin-and-tests/database"
	"github.com/marceloeduardo244/api-rest-golang-with-gin-and-tests/models"
)

// ExibeTodosAlunos godoc
// @Summary Exibir todos os alunos
// @Description Rota para exibir todos os alunos
// @Tags alunos
// @Accept json
// @Produce json
// @Success 200 {object} models.Aluno
// @Router /alunos [get]
func ExibeTodosAlunos(c *gin.Context) {
	var alunos []models.Aluno
	database.DB.Find(&alunos)
	c.JSON(200, alunos)
}

func Saudacao(c *gin.Context) {
	nome := c.Params.ByName("nome")
	c.JSON(200, gin.H{
		"API diz:": "E ai " + nome + ", tudo beleza?",
	})
}

// CriaNovoAluno godoc
// @Summary Criar um novo aluno
// @Description Rota para criar um novo aluno
// @Tags alunos
// @Accept json
// @Produce json
// @Success 200 {object} models.Aluno
// @Router /alunos [post]
func CriaNovoAluno(c *gin.Context) {
	var aluno models.Aluno
	if err := c.ShouldBindJSON(&aluno); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error()})
		return
	}
	if err := models.ValidaDadosDeCadastroAluno(&aluno); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"erro": err.Error(),
		})
		return
	}
	database.DB.Create(&aluno)
	c.JSON(http.StatusOK, aluno)
}

// BuscaAlunoPorID godoc
// @Summary Busca um aluno
// @Description Rota para Busca um aluno utilizando o ID
// @Tags alunos
// @Accept json
// @Produce json
// @Success 200 {object} models.Aluno
// @Router /alunos/{id} [get]
func BuscaAlunoPorID(c *gin.Context) {
	var aluno models.Aluno
	id := c.Params.ByName("id")
	database.DB.First(&aluno, id)

	if aluno.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"Not found": "Aluno não encontrado"})
		return
	}

	c.JSON(http.StatusOK, aluno)
}

// DeletaAluno godoc
// @Summary Apaga um aluno
// @Description Rota para Apagar um aluno utilizando o ID
// @Tags alunos
// @Accept json
// @Produce json
// @Success 200 {string} Sucesso
// @Router /alunos [delete]
func DeletaAluno(c *gin.Context) {
	var aluno models.Aluno
	id := c.Params.ByName("id")
	database.DB.Delete(&aluno, id)
	c.JSON(http.StatusOK, gin.H{"data": "Aluno deletado com sucesso"})
}

// EditaAluno godoc
// @Summary Edita um aluno
// @Description Rota para Editar um aluno utilizando o ID
// @Tags alunos
// @Accept json
// @Produce json
// @Success 200 {object} models.Aluno
// @Router /alunos/{id} [patch]
func EditaAluno(c *gin.Context) {
	var aluno models.Aluno
	id := c.Params.ByName("id")
	database.DB.First(&aluno, id)

	if err := c.ShouldBindJSON(&aluno); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error()})
		return
	}

	if err := models.ValidaDadosDeCadastroAluno(&aluno); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"erro": err.Error(),
		})
		return
	}

	database.DB.Model(&aluno).UpdateColumns(aluno)
	c.JSON(http.StatusOK, aluno)
}

func BuscaAlunoPorCPF(c *gin.Context) {
	var aluno models.Aluno
	cpf := c.Param("cpf")
	database.DB.Where(&models.Aluno{CPF: cpf}).First(&aluno)

	if aluno.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"Not found": "Aluno não encontrado"})
		return
	}

	c.JSON(http.StatusOK, aluno)
}

func ExibePaginaIndex(c *gin.Context) {
	var alunos []models.Aluno
	database.DB.Find(&alunos)
	c.HTML(
		http.StatusOK, "index.html", gin.H{
			"alunos": alunos,
		},
	)
}

func RotaNãoEncontrada(c *gin.Context) {
	c.HTML(
		http.StatusNotFound, "404.html", nil)
}
