package main

import (
	"github.com/marceloeduardo244/api-rest-golang-with-gin-and-tests/database"
	"github.com/marceloeduardo244/api-rest-golang-with-gin-and-tests/routes"
)

func main() {
	database.ConectaComBancoDeDados()
	routes.HandleRequests()
}
