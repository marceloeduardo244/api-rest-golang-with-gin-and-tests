package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/marceloeduardo244/api-rest-golang-with-gin-and-tests/controllers"
	"github.com/marceloeduardo244/api-rest-golang-with-gin-and-tests/database"
	"github.com/marceloeduardo244/api-rest-golang-with-gin-and-tests/models"
	"github.com/stretchr/testify/assert"
)

var ID int

func SetupRotasTeste() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	rotas := gin.Default()
	return rotas
}

func CriaAlunoMock() {
	aluno := models.Aluno{Nome: "Aluno Teste", CPF: "12345678911", RG: "123456789"}
	database.DB.Create(&aluno)
	ID = int(aluno.ID)
}

func DeletaAlunoMock() {
	var aluno models.Aluno
	database.DB.Delete(&aluno, ID)
}

func TestVerificaStatusCode_Saudacao_ComParametro(t *testing.T) {
	r := SetupRotasTeste()
	r.GET("/:nome", controllers.Saudacao)
	req, _ := http.NewRequest("GET", "/marcelooo", nil)
	response := httptest.NewRecorder()
	r.ServeHTTP(response, req)

	assert.Equal(t, http.StatusOK, response.Code, "Status error: valor recebido foi %d e o esperado era %d", response.Code, http.StatusOK)
	mockdaResposta := `{"API diz:":"E ai marcelooo, tudo beleza?"}`
	responseBody, _ := ioutil.ReadAll(response.Body)
	assert.Equal(t, mockdaResposta, string(responseBody), "O resultado esperado era %s, o resultado recebido foi %s", mockdaResposta, responseBody)
}

func TestListandoTodosOsAlunos(t *testing.T) {
	database.ConectaComBancoDeDados()
	r := SetupRotasTeste()
	r.GET("/alunos", controllers.ExibeTodosAlunos)
	req, _ := http.NewRequest("GET", "/alunos", nil)
	response := httptest.NewRecorder()
	r.ServeHTTP(response, req)
	assert.Equal(t, http.StatusOK, response.Code)
}

func TestBuscaALuno_PorCpf(t *testing.T) {
	database.ConectaComBancoDeDados()
	CriaAlunoMock()
	defer DeletaAlunoMock()
	r := SetupRotasTeste()
	r.GET("/alunos/cpf/:cpf", controllers.BuscaAlunoPorCPF)
	req, _ := http.NewRequest("GET", "/alunos/cpf/12345678911", nil)
	response := httptest.NewRecorder()
	r.ServeHTTP(response, req)
	assert.Equal(t, http.StatusOK, response.Code)
}

func TestBuscaALuno_PorId(t *testing.T) {
	database.ConectaComBancoDeDados()
	CriaAlunoMock()
	defer DeletaAlunoMock()
	r := SetupRotasTeste()
	r.GET("/alunos/:id", controllers.BuscaAlunoPorID)
	pathDaBusca := "/alunos/" + strconv.Itoa(ID)
	req, _ := http.NewRequest("GET", pathDaBusca, nil)
	response := httptest.NewRecorder()
	r.ServeHTTP(response, req)
	var alunoMock models.Aluno
	json.Unmarshal(response.Body.Bytes(), &alunoMock)
	assert.Equal(t, "Aluno Teste", alunoMock.Nome, "O resultado esperado era Aluno Teste, o resultado recebido foi %s", alunoMock.Nome)
	assert.Equal(t, http.StatusOK, response.Code)
}

func TestDeletaAluno(t *testing.T) {
	database.ConectaComBancoDeDados()
	CriaAlunoMock()
	r := SetupRotasTeste()
	r.DELETE("/alunos/:id", controllers.DeletaAluno)
	pathDaBusca := "/alunos/" + strconv.Itoa(ID)
	req, _ := http.NewRequest("DELETE", pathDaBusca, nil)
	response := httptest.NewRecorder()
	r.ServeHTTP(response, req)
	assert.Equal(t, http.StatusOK, response.Code)
}

func TestEditaAluno(t *testing.T) {
	database.ConectaComBancoDeDados()
	CriaAlunoMock()
	defer DeletaAlunoMock()
	r := SetupRotasTeste()
	r.PATCH("/alunos/:id", controllers.EditaAluno)

	alunoMock := models.Aluno{Nome: "Aluno Teste", CPF: "00000000000", RG: "111111111"}
	alunoJson, _ := json.Marshal(alunoMock)

	pathDaBusca := "/alunos/" + strconv.Itoa(ID)
	req, _ := http.NewRequest("PATCH", pathDaBusca, bytes.NewBuffer(alunoJson))
	response := httptest.NewRecorder()
	r.ServeHTTP(response, req)

	var alunoMockAtualizado models.Aluno
	json.Unmarshal(response.Body.Bytes(), &alunoMockAtualizado)
	assert.Equal(t, "00000000000", alunoMockAtualizado.CPF, "O resultado esperado era 00000000000, o resultado recebido foi %s", alunoMockAtualizado.CPF)
	assert.Equal(t, "111111111", alunoMockAtualizado.RG, "O resultado esperado era 111111111, o resultado recebido foi %s", alunoMockAtualizado.RG)
	assert.Equal(t, http.StatusOK, response.Code)
}
